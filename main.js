// Exercise 1
function showSmallestNum() {
  var n = 1;
  var sum = 0;

  while (sum <= 10000) {
    n++;
    sum += n;
  }

  document.getElementById("result1").innerHTML = `${n}`;
}

// Exercise 2
function calSum() {
  var x = +document.getElementById("xInput").value;
  var n = +document.getElementById("nInput").value;

  var multiply = 1;
  var sum = 0;

  for (var i = 1; i <= n; i++) {
    multiply *= x;
    sum += multiply;
  }

  document.getElementById("result2").innerHTML = `${sum}`;
}

// Exercise 3
function calMulti() {
  var n = +document.getElementById("multiInput").value;

  var multiply = 1;

  for (var i = 1; i <= n; i++) {
    multiply *= i;
  }

  document.getElementById("result3").innerHTML = `${multiply}`;
}

// Exercise 4
function showDiv() {
  var n = 10;

  var contentDiv = "";

  for (var i = 1; i <= n; i++) {
    if (i % 2 == 1) {
      contentDiv += `<div class="alert bg-primary">Div lẻ</div>`;
    } else {
      contentDiv += `<div class="alert bg-danger">Div chẵn</div>`;
    }
  }

  document.getElementById("result4").innerHTML = contentDiv;
}

// Exercise 5
function showPrimeNum() {
  var num = +document.getElementById("primeNumInput").value;

  var primeHTML = "";

  for (var i = 2; i <= num; i++) {
    var count = 0;
    for (j = 1; j <= i; j++) {
      if (i % j == 0) {
        count++;
      }
    }
    if (count == 2) {
      primeHTML += ` ${i}`;
    }
  }

  document.getElementById("result5").innerHTML = `${primeHTML}`;
}
